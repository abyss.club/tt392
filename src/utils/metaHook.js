import { useEffect } from 'react';

/**
 * Hook for changing document meta.
 *
 * @param {object} params - Params.
 * @param {string} params.title - Document title.
 * @param {string} params.content - Document meta.
 */
function useMeta({ title = '', content = '' }) {
  useEffect(() => {
    const oldTitle = document.title;
    const oldDescription = document.querySelector('meta[name=description]').content;
    if (title) {
      const newTitle = `${title} - Abyss`;
      document.title = newTitle;
      document.querySelector("meta[property='og:title']").content = newTitle;
    }
    if (content) {
      document.querySelector('meta[name=description]').content = content;
      document.querySelector("meta[property='og:description']").content = content;
    }
    return () => {
      if (title) {
        document.title = oldTitle;
        document.querySelector("meta[property='og:title']").content = oldTitle;
      }
      if (content) {
        document.querySelector('meta[name=description]').content = oldDescription;
        document.querySelector("meta[property='og:description']").content = oldDescription;
      }
    };
  }, [content, title]);
}

export default useMeta;
