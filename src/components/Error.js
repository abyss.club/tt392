import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  FORBIDDEN, NOT_FOUND, INTERNAL_ERROR, PARAMS_ERROR, NETWORK_ERROR,
} from 'utils/errorCodes';
import MainContent from 'styles/MainContent';
import useMeta from 'utils/metaHook';

const ErrorPage = ({ match }) => {
  const errCode = match.params.errCode || 'NOT_FOUND';
  const errMsgList = {
    [FORBIDDEN]: 'You are not allowed to access the content.',
    [NOT_FOUND]: 'Requested content not found.',
    [INTERNAL_ERROR]: 'We screwed up.',
    [PARAMS_ERROR]: 'Server screwed up.',
    UNKNOWN_ERROR: 'Unknown error occured.',
    [NETWORK_ERROR]: 'Remote server unreachable.',
  };

  useEffect(() => {
    const oldTitle = document.title;
    const oldDescription = document.querySelector('meta[name=description]').content;
    const newTitle = 'Error occured.';
    document.title = newTitle;
    document.querySelector('meta[name=description]').content = 'Error occured.';
    document.querySelector("meta[property='og:title']").content = newTitle;
    document.querySelector("meta[property='og:description']").content = 'Error occured.';
    return () => {
      document.title = oldTitle;
      document.querySelector('meta[name=description]').content = oldDescription;
      document.querySelector("meta[property='og:description']").content = oldDescription;
    };
  }, []);

  useMeta({ title: 'Error occured.', content: 'Error occured.' });

  return (
    <MainContent>
      <p>
        Error occured:
        {' '}
        {errMsgList[errCode]}
      </p>
    </MainContent>
  );
};
ErrorPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      errCode: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default ErrorPage;
