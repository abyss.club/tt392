import React, {
  useContext, useEffect, useState, useCallback,
} from 'react';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';

import TagsContext from 'providers/Tags';
import LoginContext from 'providers/Login';
import { useLoadingBar } from 'styles/Loading';
import { useRouter } from 'utils/routerHooks';
import { UNAUTHENTICATED, NETWORK_ERROR, UNKNOWN_ERROR } from 'utils/errorCodes';

const Tags = () => {
  const { history } = useRouter();

  const handleOnErr = useCallback((e) => {
    if (e.networkError) {
      history.push('/error/NETWORK_ERROR');
    } else {
      history.push('/error/UNKNOWN_ERROR');
    }
  }, [history]);

  const { loading, data, error } = useQuery(TAGS, { onError: handleOnErr });
  const [, dispatchTags] = useContext(TagsContext);
  const [, { startLoading, stopLoading }] = useLoadingBar();

  useEffect(() => {
    if (loading) {
      startLoading();
    }
    if (!loading && !error) {
      const { mainTags, recommended } = data;
      dispatchTags({ type: 'INIT', tags: { mainTags, recommended } });
      stopLoading();
    }
  }, [data, loading, error, startLoading, stopLoading, dispatchTags]);
  return null;
};
Tags.whyDidYouRender = true;

const Login = () => {
  const [{ initialized }, dispatchLogin] = useContext(LoginContext);
  const [, dispatchTags] = useContext(TagsContext);
  const [, { startLoading, stopLoading }] = useLoadingBar();
  const [errCode, setErrCode] = useState('');

  const handleOnErr = useCallback((e) => {
    if (Array.isArray(e.graphQLErrors) && e.graphQLErrors.length > 0) {
      setErrCode(e.graphQLErrors[0].extensions.code);
    } else if (e.networkError) {
      setErrCode(NETWORK_ERROR);
    } else {
      setErrCode(UNKNOWN_ERROR);
    }
  }, []);
  const { loading, data } = useQuery(PROFILE, { onError: handleOnErr });

  useEffect(() => {
    if (!initialized) {
      if (loading) {
        startLoading();
      }
      if (!loading) {
        if (errCode === UNAUTHENTICATED) {
          dispatchLogin({ type: 'INIT' });
          stopLoading();
        }
        if (data && data.profile) {
          const { profile, mainTags, recommended } = data;
          dispatchLogin({
            type: 'INIT_WITH_LOGIN',
            profile,
          });
          dispatchTags({
            type: 'INIT_WITH_LOGIN',
            profile,
            tags: { mainTags, recommended },
          });
          stopLoading();
        }
      }
    }
  }, [data, loading, errCode, startLoading, stopLoading, dispatchLogin, dispatchTags, initialized]);

  return null;
};
Login.whyDidYouRender = true;

const Init = () => (
  <>
    <Tags />
    <Login />
  </>
);

const PROFILE = gql`
  query {
    profile {
      name
      email
      tags
      role
    }
    mainTags
    recommended
  }
`;

const TAGS = gql`
  query {
    mainTags
    recommended
  }
`;

export default Init;
