import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';
import ThreadList from 'components/ThreadList';
import MainContent from 'styles/MainContent';
import useMeta from 'utils/metaHook';

const Wrapper = styled(MainContent)`
  display: flex;
  flex-flow: column;
`;

const TagView = ({ match }) => {
  useMeta({ title: `#${match.params.slug}`, content: `在 Abyss 查看关于 ${match.params.slug} 的所有帖子。` });

  return (
    <Wrapper>
      <ThreadList type="tag" slug={match.params.slug} />
    </Wrapper>
  );
};

TagView.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      slug: PropTypes.string,
    }),
  }).isRequired,
};

export default TagView;
